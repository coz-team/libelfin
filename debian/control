Source: libelfin
Section: libdevel
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 12)
 , dh-exec (>= 0.3)
 , g++ (>= 4.7)
 , python3-minimal
 , pkgconf
Standards-Version: 4.7.0
Homepage: https://github.com/aclements/libelfin
Vcs-Browser: https://salsa.debian.org/debian/libelfin
Vcs-Git: https://salsa.debian.org/debian/libelfin.git

# NOTE: We provide separate "libelf++" and "libdwarf++" packages to let their
# libraries evolve their ABI / SONAME independently.

Package: libelf++0t64
Provides: ${t64:Provides}
Replaces: libelf++0
Breaks: libelf++0 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: C++11 ELF parser
 Libelfin is a from-scratch C++11 library for reading ELF binaries and DWARFv4
 debug information.
 .
 Libelfin implements a syntactic layer for DWARF and ELF, but not a semantic
 layer. Interpreting the information stored in DWARF DIE trees still requires a
 great deal of understanding of DWARF, but libelfin will make sense of the bytes
 for you.
 .
 This package contains the library to parse ELF binaries.

Package: libdwarf++0t64
Provides: ${t64:Provides}
Replaces: libdwarf++0
Breaks: libdwarf++0 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}, libelf++0t64 (= ${binary:Version})
Description: C++11 DWARF parser
 Libelfin is a from-scratch C++11 library for reading ELF binaries and DWARFv4
 debug information.
 .
 Libelfin implements a syntactic layer for DWARF and ELF, but not a semantic
 layer. Interpreting the information stored in DWARF DIE trees still requires a
 great deal of understanding of DWARF, but libelfin will make sense of the bytes
 for you.
 .
 This package contains the library to parse DWARFv4 debug information.

Package: libelfin-dev
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}, libdwarf++0t64 (= ${binary:Version})
Description: C++11 ELF/DWARF parser (development files)
 Libelfin is a from-scratch C++11 library for reading ELF binaries and DWARFv4
 debug information.
 .
 Libelfin implements a syntactic layer for DWARF and ELF, but not a semantic
 layer. Interpreting the information stored in DWARF DIE trees still requires a
 great deal of understanding of DWARF, but libelfin will make sense of the bytes
 for you.
 .
 This package contains development libraries and header files for libelfin.
